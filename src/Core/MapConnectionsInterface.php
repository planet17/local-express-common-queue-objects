<?php

namespace LocalExpress\CommonQueueObjects\Bundles\Core;

/**
 * Interface MapConnectionsInterface
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\Core
 */
interface MapConnectionsInterface
{
    public const CONNECTION_GEARMAN_DOCKER = 'gearman-docker';
}
