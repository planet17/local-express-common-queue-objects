<?php

namespace LocalExpress\CommonQueueObjects\Bundles\Core\Routes;

use LocalExpress\CommonQueueObjects\Bundles\Core\MapConnectionsInterface;
use Planet17\MessageQueueLibrary\Routes\ConnectableRoute;

/**
 * Class GearmanRoute
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\Core\Routes
 */
abstract class GearmanRoute extends ConnectableRoute
{
    protected $connectionName = MapConnectionsInterface::CONNECTION_GEARMAN_DOCKER;
}
