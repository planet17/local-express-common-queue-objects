<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer;

/**
 * Class ConfigurationDto
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer
 */
class ConfigurationDto
{
    /** @var $amountActiveHandlerProcessing */
    private $amountActiveHandlerProcessing;

    /** @var $amountDefaultProcessingParallelImportPerCompany */
    private $amountDefaultProcessingParallelImportPerCompany;

    /**
     * ImportProcessingBalancerConfigurationDto constructor.
     *
     * @param int $amountActiveHandlerProcessing
     * @param int|null $amountDefaultProcessingParallelImportPerCompany
     */
    public function __construct(
        int $amountActiveHandlerProcessing,
        ?int $amountDefaultProcessingParallelImportPerCompany = null
    ) {
        $this->amountActiveHandlerProcessing = $amountActiveHandlerProcessing;
        $this->amountDefaultProcessingParallelImportPerCompany = $amountDefaultProcessingParallelImportPerCompany;
    }

    /**
     * Getter amount of running handlers for FileProcessing.
     *
     * @return int
     */
    public function getAmountActiveHandlerProcessing(): int
    {
        return $this->amountActiveHandlerProcessing;
    }

    /**
     * Getter default number of files from one store could be processed in parallel.
     *
     * @return int|null
     */
    public function getAmountDefaultProcessingParallelImportPerCompany(): ?int
    {
        return $this->amountDefaultProcessingParallelImportPerCompany;
    }
}
