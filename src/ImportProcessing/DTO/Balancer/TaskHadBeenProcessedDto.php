<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer;

/**
 * Class TaskHadBeenProcessedDto
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer
 */
class TaskHadBeenProcessedDto
{
    /** @var int $importIdentifier */
    private $importIdentifier;

    /** @var int $storeIdentifier */
    private $storeIdentifier;

    /**
     * FileProcessingTaskDto constructor.
     *
     * @param int $importIdentifier
     * @param int $storeIdentifier
     */
    public function __construct(int $importIdentifier, int $storeIdentifier)
    {
        $this->importIdentifier = $importIdentifier;
        $this->storeIdentifier = $storeIdentifier;
    }

    /**
     * Getter store identifier.
     *
     * @return mixed
     */
    public function getStoreIdentifier(): int
    {
        return $this->storeIdentifier;
    }

    /**
     * Getter import identifier.
     *
     * @return mixed
     */
    public function getImportIdentifier(): int
    {
        return $this->importIdentifier;
    }
}
