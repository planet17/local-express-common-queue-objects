<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer;

/**
 * Class TaskNewDto
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer
 */
class TaskNewDto
{
    /** @var int $importIdentifier */
    private $importIdentifier;

    /** @var int $storeIdentifier */
    private $storeIdentifier;

    /** @var string $filePath */
    private $filePath;

    /** @var int $storeProcessingParallelLimit */
    private $storeProcessingParallelLimit;

    /**
     * FileProcessingTaskDto constructor.
     *
     * @param int $importIdentifier
     * @param int $storeIdentifier
     * @param string $filePath
     * @param int $storeProcessingParallelLimit
     */
    public function __construct(
        int $importIdentifier,
        int $storeIdentifier,
        string $filePath,
        int $storeProcessingParallelLimit = 1
    ) {
        $this->importIdentifier = $importIdentifier;
        $this->storeIdentifier = $storeIdentifier;
        $this->filePath = $filePath;
        $this->storeProcessingParallelLimit = $storeProcessingParallelLimit;
    }

    /**
     * Getter import identifier.
     *
     * @return mixed
     */
    public function getImportIdentifier(): int
    {
        return $this->importIdentifier;
    }

    /**
     * Getter store identifier.
     *
     * @return mixed
     */
    public function getStoreIdentifier(): int
    {
        return $this->storeIdentifier;
    }

    /**
     * Getter full path to file for reading and processing.
     *
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * Getter store processing parallel process limit.
     *
     * @return int
     */
    public function getStoreProcessingParallelLimit(): int
    {
        return $this->storeProcessingParallelLimit;
    }
}
