<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO;

/**
 * Class ImportProductStoringDto
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO
 */
class ImportProductStoringDto
{
    /** @var int $importIdentifier */
    private $importIdentifier;

    /** @var int $storeIdentifier */
    private $storeIdentifier;

    /** @var string $item */
    private $item;

    /**
     * FileProcessingTaskDto constructor.
     *
     * @param int $importIdentifier
     * @param int $storeIdentifier
     * @param mixed|ProductDto|ProductMakingExceptionDto $item
     */
    public function __construct(int $importIdentifier, int $storeIdentifier, $item)
    {
        $this->importIdentifier = $importIdentifier;
        $this->storeIdentifier = $storeIdentifier;
        $this->item = $item;
    }

    /**
     * Getter import identifier.
     *
     * @return mixed
     */
    public function getImportIdentifier(): int
    {
        return $this->importIdentifier;
    }

    /**
     * Getter store identifier.
     *
     * @return mixed
     */
    public function getStoreIdentifier(): int
    {
        return $this->storeIdentifier;
    }

    /**
     * Getter item from string or some error\exception while extracting.
     *
     * @return mixed|ProductDto|ProductMakingExceptionDto
     */
    public function getItem()
    {
        return $this->item;
    }
}
