<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO;

use Exception;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\FileReadingException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\InvalidTableException;

/**
 * Class ImportStateUpdatingDto
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO
 */
class ImportStateUpdatingDto
{
    /** @var int $importIdentifier */
    private $importIdentifier;

    /** @var int $storeIdentifier */
    private $storeIdentifier;

    /** @var int $state */
    private $state;

    /** @var FileReadingException|InvalidTableException|null */
    private $exception;

    /** @var int|null */
    private $countTotalProducts;

    /**
     * FileProcessingTaskDto constructor.
     *
     * @param int $importIdentifier
     * @param int $storeIdentifier
     * @param int|null $countTotalProducts
     * @param int $stateId
     * @param Exception|null $exception
     */
    public function __construct(
        int $importIdentifier,
        int $storeIdentifier,
        ?int $countTotalProducts,
        int $stateId,
        ?Exception $exception = null
    ) {
        $this->importIdentifier = $importIdentifier;
        $this->storeIdentifier = $storeIdentifier;
        $this->countTotalProducts = $countTotalProducts;
        $this->state = $stateId;
        $this->exception = $exception;
    }

    /**
     * @return int
     */
    public function getImportIdentifier(): int
    {
        return $this->importIdentifier;
    }

    /**
     * @return int
     */
    public function getStoreIdentifier(): int
    {
        return $this->storeIdentifier;
    }

    /**
     * @return int|null
     */
    public function getCountTotalProducts(): ?int
    {
        return $this->countTotalProducts;
    }

    /**
     * @param int|null $countTotalProducts
     *
     * @return ImportStateUpdatingDto
     */
    public function setCountTotalProducts(?int $countTotalProducts): ImportStateUpdatingDto
    {
        $this->countTotalProducts = $countTotalProducts;

        return $this;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     *
     * @return ImportStateUpdatingDto
     */
    public function setState(int $state): ImportStateUpdatingDto
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return InvalidTableException|FileReadingException|null
     */
    public function getException(): ?Exception
    {
        return $this->exception;
    }

    /**
     * @param InvalidTableException|FileReadingException|Exception|null $exception
     *
     * @return ImportStateUpdatingDto
     */
    public function setException(?Exception $exception): ImportStateUpdatingDto
    {
        $this->exception = $exception;

        return $this;
    }
}
