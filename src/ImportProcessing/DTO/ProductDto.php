<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO;

/**
 * Class ProductDto
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO
 */
class ProductDto
{
    /** @var string $upc */
    private $upc;

    /** @var float $price */
    private $price;

    /** @var string|null $title */
    private $title;

    /**
     * ProductDto constructor.
     *
     * @param string $upc
     * @param float $price
     * @param string|null $title
     */
    public function __construct(string $upc, float $price, ?string $title = null)
    {
        $this->upc = $upc;
        $this->price = $price;
        $this->title = $title;
    }

    /**
     * Getter UPC-number.
     *
     * @return string
     */
    public function getUpc(): string
    {
        return $this->upc;
    }

    /**
     * Getter Price.
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Getter Title.
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
}
