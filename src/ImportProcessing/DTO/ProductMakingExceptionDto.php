<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO;

/**
 * Class ProductMakingExceptionDto
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO
 */
class ProductMakingExceptionDto
{
    /** @var string $exceptionMessage */
    private $exceptionMessage;

    /**
     * ProductMakingExceptionDto constructor.
     *
     * @param string $exceptionMessage
     */
    public function __construct(string $exceptionMessage)
    {
        $this->exceptionMessage = $exceptionMessage;
    }

    /**
     * @return string
     */
    public function getExceptionMessage(): string
    {
        return $this->exceptionMessage;
    }
}
