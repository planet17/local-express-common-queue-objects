<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions;

use RuntimeException;
use Throwable;

/**
 * Class InvalidTableException
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions
 */
class InvalidTableException extends RuntimeException
{
    /** @const EXCEPTION_MESSAGE */
    public const EXCEPTION_MESSAGE = 'Invalid table, make sure columns is right';

    /** @var int $countFailedRows */
    private $countFailedRows = 0;

    /**
     * InvalidTableException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        if (is_numeric($message)) {
            $this->countFailedRows = $message;
            $message = self::EXCEPTION_MESSAGE;
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * Getter of count for failed rows at the file with table.
     *
     * @return int
     */
    public function getCountFailedRows():int
    {
        return $this->countFailedRows;
    }
}
