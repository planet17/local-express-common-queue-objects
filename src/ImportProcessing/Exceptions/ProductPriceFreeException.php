<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions;

use InvalidArgumentException;
use Throwable;

/**
 * Class ProductPriceFreeException
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions
 */
class ProductPriceFreeException extends InvalidArgumentException
{
    /** @const DEFAULT_MESSAGE */
    protected const DEFAULT_MESSAGE = 'Price does not has to be a zero or negative.';

    /**
     * ProductPriceFreeException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::DEFAULT_MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
