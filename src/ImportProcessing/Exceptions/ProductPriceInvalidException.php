<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions;

use InvalidArgumentException;
use Throwable;

/**
 * Class ProductPriceInvalidException
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions
 */
class ProductPriceInvalidException extends InvalidArgumentException
{
    /** @const DEFAULT_MESSAGE */
    protected const DEFAULT_MESSAGE = 'Price must be a float.';

    /**
     * ProductPriceFreeException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::DEFAULT_MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
