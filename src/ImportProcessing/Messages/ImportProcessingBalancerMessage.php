<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportProcessingBalancerRoute;
use Planet17\MessageQueueLibrary\Messages\BaseMessage;

/**
 * Class ImportProcessingBalancerMessage
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages
 */
class ImportProcessingBalancerMessage extends BaseMessage
{
    protected $routeClass = ImportProcessingBalancerRoute::class;

    private $dto;

    /**
     * Message constructor.
     *
     * @param null|mixed $payload
     *
     * @noinspection MagicMethodsValidityInspection
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct($payload = null)
    {
        $this->dto = $payload;
    }

    /**
     * Getter for DTO.
     *
     * @return null|mixed
     */
    public function getDto()
    {
        return $this->dto;
    }

    /**
     * Override.
     *
     * @return string
     */
    public function getPayload(): string
    {
        return serialize($this);
    }
}
