<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportProductStoringDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportProductStoringRoute;
use Planet17\MessageQueueLibrary\Messages\BaseMessage;
use Planet17\MessageQueueProcessManager\Exception\WrongDTOProvidedException;

/**
 * Class ImportProductStoringMessage
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages
 */
class ImportProductStoringMessage extends BaseMessage
{
    protected $routeClass = ImportProductStoringRoute::class;

    /** @var ImportProductStoringDto */
    private $dto;

    /**
     * Message constructor.
     *
     * @param null|ImportProductStoringDto $payload
     *
     * @throws WrongDTOProvidedException
     *
     * @noinspection MagicMethodsValidityInspection
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct($payload = null)
    {
        if ($payload !== null && !($payload instanceof ImportProductStoringDto)) {
            throw new WrongDTOProvidedException;
        }

        $this->dto = $payload;
    }

    /**
     * Getter for DTO.
     *
     * @return ImportProductStoringDto
     */
    public function getDto(): ImportProductStoringDto
    {
        return $this->dto;
    }

    /**
     * Override.
     *
     * @return string
     */
    public function getPayload(): string
    {
        return serialize($this);
    }
}
