<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportStateUpdatingDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportStateUpdatingRoute;
use Planet17\MessageQueueLibrary\Messages\BaseMessage;
use Planet17\MessageQueueProcessManager\Exception\WrongDTOProvidedException;

/**
 * Class ImportStateUpdatingMessage
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages
 */
class ImportStateUpdatingMessage extends BaseMessage
{

    /** @var string  */
    protected $routeClass = ImportStateUpdatingRoute::class;

    /** @var ImportStateUpdatingDto */
    private $dto;

    /**
     * Message constructor.
     *
     * @param null|ImportStateUpdatingDto $payload
     *
     * @throws WrongDTOProvidedException
     *
     * @noinspection MagicMethodsValidityInspection
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct($payload = null)
    {
        if ($payload !== null && !($payload instanceof ImportStateUpdatingDto)) {
            throw new WrongDTOProvidedException;
        }

        $this->dto = $payload;
    }

    /**
     * Getter for DTO.
     *
     * @return ImportStateUpdatingDto
     */
    public function getDto(): ImportStateUpdatingDto
    {
        return $this->dto;
    }

    /**
     * Override.
     *
     * @return string
     */
    public function getPayload(): string
    {
        return serialize($this);
    }
}
