<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes;

use LocalExpress\CommonQueueObjects\Bundles\Core\Routes\GearmanRoute;

/**
 * Class ApplicationCallbackOnInitRoute
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes
 */
class ApplicationCallbackOnInitRoute extends GearmanRoute
{
    /** @inheritdoc */
    public function getAliasShort(): string
    {
        return 'import-processing-application-callback';
    }
}
