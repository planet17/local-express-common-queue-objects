<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes;

use LocalExpress\CommonQueueObjects\Bundles\Core\Routes\GearmanRoute;

/**
 * Class ImportProcessingBalancerRoute
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes
 */
class ImportProcessingBalancerRoute extends GearmanRoute
{
    /** @inheritdoc */
    public function getAliasShort(): string
    {
        return 'import-processing-balancer';
    }
}
