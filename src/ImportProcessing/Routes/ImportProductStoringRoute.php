<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes;

use LocalExpress\CommonQueueObjects\Bundles\Core\Routes\GearmanRoute;

/**
 * Class ImportProductStoringRoute
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes
 */
class ImportProductStoringRoute extends GearmanRoute
{
    /** @inheritdoc  */
    public function getAliasShort(): string
    {
        return 'product-storing-application';
    }
}
