<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes;

use LocalExpress\CommonQueueObjects\Bundles\Core\Routes\GearmanRoute;

/**
 * Class ImportStateUpdatingRoute
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes
 */
class ImportStateUpdatingRoute extends GearmanRoute
{
    /**
     * @return string
     */
    public function getAliasShort(): string
    {
        return 'import-state-updating';
    }
}
