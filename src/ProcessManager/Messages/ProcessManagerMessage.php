<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Messages;

use LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Routes\ProcessManagerRoute;

/**
 * Class ProcessManagerMessage
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Messages
 */
class ProcessManagerMessage extends \Planet17\MessageQueueProcessManager\Messages\ProcessManagerMessage
{
    protected $routeClass = ProcessManagerRoute::class;
}
