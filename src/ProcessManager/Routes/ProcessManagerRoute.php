<?php

namespace LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Routes;


use LocalExpress\CommonQueueObjects\Bundles\Core\MapConnectionsInterface;

/**
 * Class ProcessManagerRoute
 *
 * @package LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Routes
 */
class ProcessManagerRoute extends \Planet17\MessageQueueProcessManager\Routes\ProcessManagerRoute
{
    protected $connectionName = MapConnectionsInterface::CONNECTION_GEARMAN_DOCKER;
}
